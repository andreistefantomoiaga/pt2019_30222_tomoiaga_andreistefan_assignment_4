package BusinessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem  implements Serializable {

    public BaseProduct(String productName,float price, float kcal,int grammage){
        this.setProductName(productName);
        this.setPrice(price);
        this.setKcal(kcal);
        this.setGrammage(grammage);
    }
    public BaseProduct(){ }

    @Override
    public float computePrice() {
        return this.getPrice();
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public float getKcal() {
        return super.getKcal();
    }

    @Override
    public float getPrice() {
        return super.getPrice();
    }

    @Override
    public int getGrammage() {
        return super.getGrammage();
    }

    @Override
    public String getProductName() {
        return super.getProductName();
    }
}
