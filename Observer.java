package BusinessLayer;

public abstract class Observer {

    private Restaurant subject;

    public abstract void update(Order order);

    public void setRestaurant(Restaurant subject) {
        this.subject = subject;
    }
}
