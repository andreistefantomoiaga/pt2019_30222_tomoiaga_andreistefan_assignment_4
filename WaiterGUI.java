package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WaiterGUI extends JFrame {

   JPanel mainPanel = new JPanel(new GridLayout(1, 2));

    JPanel buttonPanel;
    JPanel tablePanel;

    JButton newOrder;
    JButton viewAllOrders;
    JButton computeBill;

    JLabel orderID = new JLabel("Order ID:");
    JTextField orderTf = new JTextField("",3);

    JScrollPane scrollPane;
    JTable orderTable;


    public WaiterGUI(){

        this.setTitle("Waiter");
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.Y_AXIS));
        tablePanel = new JPanel();
        newOrder = new JButton("New Order");
        viewAllOrders = new JButton("View All Orders");
        computeBill = new JButton("Compute bill");

        DefaultTableModel tableModel = new DefaultTableModel(new String[][]{ {"1","3"},{"2","3"}},new String[] {"1","2"});

        orderTable = new JTable(tableModel);
        scrollPane = new JScrollPane(orderTable);

        init();

        this.setSize(1000,500);

        this.setLocation(900,0);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    private void init(){

        others();

        scrollPane.add(orderTable);
        tablePanel.add(scrollPane);

        mainPanel.add(buttonPanel);
        mainPanel.add(tablePanel);
        add(mainPanel);
    }
    private void others(){

        buttonPanel.add(Box.createVerticalStrut(100));
        buttonPanel.add(newOrder);
        buttonPanel.add(Box.createVerticalStrut(30));
        buttonPanel.add(viewAllOrders);
        buttonPanel.add(Box.createVerticalStrut(30));
        JPanel panel1 = new JPanel(new FlowLayout());
        buttonPanel.add(panel1);
        panel1.add(computeBill);
        panel1.add(orderID);
        panel1.add(orderTf);


        newOrder.setAlignmentX(Component.CENTER_ALIGNMENT);
        viewAllOrders.setAlignmentX(Component.CENTER_ALIGNMENT);
        computeBill.setAlignmentX(Component.CENTER_ALIGNMENT);


        buttonPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        tablePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        scrollPane.setSize(300,300);
        orderTable.setSize(300,300);


    }

    public void refresh(Map<Order,List<MenuItem>> hashmap){

        System.out.println("Clicked");
        tablePanel.removeAll();

        orderTable  = createTable(hashmap);
        scrollPane = new JScrollPane(orderTable);
        tablePanel.add(scrollPane);

        tablePanel.revalidate();
        tablePanel.repaint();

    }

    public JTable createTable(Map<Order,List<MenuItem>> hashmap){


        Set<Order> orderSet = hashmap.keySet();

        List<String[]> data = new ArrayList<String[]>();

        List<String> header = new ArrayList<String>();
        header.add("OrderId");
        header.add("Date");
        header.add("Table No");
        header.add("$");


        for (Order key : orderSet) {

            float price = 0;
            List<MenuItem> orderItems = hashmap.get(key);

            for (MenuItem  item : orderItems) {
                price = item.computePrice();
            }
            String priceString = String.valueOf(price);
            data.add(new String[]{String.valueOf(key.getOrderId()), key.getDateString(),String.valueOf(key.getTable()),String.valueOf(hashmap.get(key)), priceString});
        }


        TableModel tableModel= new DefaultTableModel(data.toArray(new Object[][]{}),header.toArray());
        JTable retunedJtable =new JTable(tableModel);

        return retunedJtable;
    }

    public void newOrder(ActionListener e){
        newOrder.addActionListener(e);
    }
    public void viewOrders(ActionListener e){viewAllOrders.addActionListener(e);}
    public void compute(ActionListener e) {computeBill.addActionListener(e);}

    public int getOrderID(){
        return Integer.parseInt(orderTf.getText());
    }

    public static void main(String[] args) {
        WaiterGUI wgui = new WaiterGUI();
    }
}
