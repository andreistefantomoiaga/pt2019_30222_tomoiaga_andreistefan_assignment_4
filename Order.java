package BusinessLayer;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Order implements Serializable {

    public int orderId;
    public String dateString;
    public SimpleDateFormat dateFormat;
    public int table;

    public Order(){
        orderId = super.hashCode();
        table = orderId % 100 ;
        dateFormat = new SimpleDateFormat("dd MMM HH:mm");
        dateString = dateFormat.format(new Date());
        try {
            dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getOrderId() {
        return orderId;
    }

    @Override
    public int hashCode() {
        return (orderId+table)% 100;
    }

    public int getTable() {
        return table;
    }

    public String getDateString() {
        return dateString;
    }

    public static void main(String[] args) {
        Order order = new Order();
        System.out.println(order.dateString);
    }
}
