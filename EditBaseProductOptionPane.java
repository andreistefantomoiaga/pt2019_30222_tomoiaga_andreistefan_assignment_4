package PresentationLayer;

import javax.swing.*;
import java.awt.*;

public class EditBaseProductOptionPane extends JFrame {

    JPanel panel = new JPanel(new GridLayout(6,0));

    JLabel idlab = new JLabel("BaseProduct ID:");
    JLabel name = new JLabel("ProductName: ");
    JLabel price = new JLabel("Price: ");
    JLabel kcal = new JLabel("KCAL: ");
    JLabel grammage = new JLabel("Grammage: ");

    JTextField gramTF = new JTextField("",5);
    JTextField idTF = new JTextField("",5);
    JTextField kcalTF = new JTextField("0",5);
    JTextField priceTF = new JTextField("0",5);
    JTextField nameTF = new JTextField("",5);

    JButton edit = new JButton("Edit");
    JButton exit = new JButton("Exit");

    public EditBaseProductOptionPane(){

        panel.add(idlab);
        panel.add(idTF);
        panel.add(name);
        panel.add(nameTF);
        panel.add(price);
        panel.add(priceTF);
        panel.add(kcal);
        panel.add(kcalTF);
        panel.add(grammage);
        panel.add(gramTF);

        panel.add(edit);
        panel.add(exit);



        this.add(panel);

        Dimension baseProdDim = new Dimension(400,150);
        this.setSize(baseProdDim);
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JButton getEdit() {
        return edit;
    }
    public JButton getExit() {
        return exit;
    }

    public int getId(){
        return Integer.parseInt(idTF.getText());
    }
    public String getProductName(){
        return nameTF.getText();
    }
    public float getPrice(){
        return Float.parseFloat(priceTF.getText());
    }



    public static void main(String[] args) {
        new EditBaseProductOptionPane();
    }
}
