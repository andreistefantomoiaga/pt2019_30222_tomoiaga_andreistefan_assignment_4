package BusinessLayer;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem implements Serializable {

    private List<MenuItem> productComponents;

    public CompositeProduct(){
        productComponents = new ArrayList<MenuItem>();
    }

    public CompositeProduct(String compositeProductName){
        productComponents = new ArrayList<MenuItem>();
        this.setProductName(compositeProductName);

    }

    public void addComponent(MenuItem component){
        productComponents.add(component);
        //this.setPrice(this.getPrice()+component.getPrice());
    }

    public void removeComponent(MenuItem component){
        productComponents.remove(component);
    }

    @Override
    public float getKcal() {
        if (!productComponents.isEmpty()){

            float computedCal=0;

            for (MenuItem menuComponent: productComponents) {
                computedCal += menuComponent.getKcal();
            }
            return computedCal;

        }else{
            return 0;
        }
    }

    @Override
    public int getGrammage() {

        if (!productComponents.isEmpty()){

            int computedGrammage=0;

            for (MenuItem menuComponent: productComponents) {
                computedGrammage += menuComponent.getGrammage();
            }
            return computedGrammage;

        }else{
            return 0;
        }
    }

    @Override
    public float computePrice() {

        if (!productComponents.isEmpty()){

            float computedPrice=0;

            for (MenuItem menuComponent: productComponents) {
                computedPrice += menuComponent.getPrice();
            }
            return computedPrice;

        }else{
            return 0;
        }

    }

    @Override
    public float getPrice() {
        return computePrice();
    }

    public static void main(String[] args) {

        MenuItem item1 = new CompositeProduct("Hamburger Menu");
        MenuItem item2 = new BaseProduct();
        MenuItem item3 = new BaseProduct();
        MenuItem item4 = new BaseProduct();

        item2.setProductName("Hamburger");
        item3.setProductName("French Fries");
        item4.setProductName("Coke");

        item2.setPrice(4);
        item3.setPrice(3);
        item4.setPrice(2);

        item2.setGrammage(100);
        item3.setGrammage(50);
        item4.setGrammage(70);

        item2.setKcal(254F);
        item3.setKcal(311.9F);
        item4.setKcal(139F);

        ((CompositeProduct) item1).addComponent(item2);
        ((CompositeProduct) item1).addComponent(item3);
        ((CompositeProduct) item1).addComponent(item4);

        System.out.println("Item 1: " +item1.getProductName() + " priced " + item1.computePrice());
        System.out.println("Item 2: " + item2.getProductName()+" priced "+ item2.getPrice()+" with E.V "+ item2.getKcal()+" kcal");
        System.out.println(item1.getKcal());
        System.out.println(item1.getGrammage());
    }

}
