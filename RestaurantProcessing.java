package BusinessLayer;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface RestaurantProcessing {

    /*
    *   Administrator operations:
    * */
    void createBaseProduct(String productName, float price);
    int createCompositeProduct(String productName);
    MenuItem searchProduct(int id);
    void deleteMenuItem(int id);
    void editMenuItem(int id,String editProductName, float editPrice, float editKcal, int editGrammage );
    void removeFromComposite(int id,MenuItem removedItem);
    //this should be written again

    /*
    *   Waiter operations:
    * */
    Order createNewOrder();
    void computeBill(Order ord) throws FileNotFoundException, UnsupportedEncodingException;
    int computePrice(Order order);
    void generateBill(Order order);

}
