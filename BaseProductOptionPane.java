package PresentationLayer;

import javax.swing.*;
import java.awt.*;

public class BaseProductOptionPane extends JFrame {

    JPanel panel = new JPanel(new GridLayout(2,0));
    JPanel secondPanel = new JPanel(new GridLayout(2,2));
    JLabel itemName = new JLabel("Item Name: ");
    JTextField itemTF = new JTextField();
    JLabel price = new JLabel("$:");
    JTextField priceTF= new JTextField();
    JButton create = new JButton("Create");

    public BaseProductOptionPane(){




        secondPanel.add(itemName,0);
        secondPanel.add(itemTF,1);
        secondPanel.add(price,0);
        secondPanel.add(priceTF,1);

        panel.add(secondPanel,0);
        panel.add(create,1);
        this.add(panel);

        Dimension baseProdDim = new Dimension(230,150);
        this.setSize(baseProdDim);
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        BaseProductOptionPane bs= new BaseProductOptionPane();

    }

    public String getItemName() {
        return itemTF.getText();
    }

    public float getPrice() {
        return Float.parseFloat(priceTF.getText());
    }

    public JButton getCreate() {
        return create;
    }
}
