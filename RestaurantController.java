package Controller;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerializator;
import PresentationLayer.*;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RestaurantController {

    private AdministratorGUI adminView;
    private WaiterGUI waiterView;
    private ChefGUI chefView;
    private Restaurant restaurantLogic;
    private RestaurantSerializator restaurantSerializator;

    public RestaurantController() throws IOException, ClassNotFoundException {

        restaurantSerializator = new RestaurantSerializator();
        restaurantSerializator.deserializeRestaurant();

        adminView = new AdministratorGUI();
        waiterView = new WaiterGUI();
        chefView = new ChefGUI();
        restaurantLogic = restaurantSerializator.getRestaurant();
        chefView.setRestaurant(restaurantLogic);
        restaurantLogic.setObserver(chefView);

        adminView.newItem(new newItemListener());
        adminView.editItem(new editItemListener());
        adminView.deleteItem(new deleteItemListener());
        adminView.viewItem(new viewItemListener());
        adminView.serialize(new serializeListener());

        waiterView.newOrder(new newOrderListener());
        waiterView.viewOrders(new orderViewListener());
        waiterView.compute(new computeListener());

    }

    private class serializeListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            try {
                restaurantSerializator.serializeRestaurant(restaurantLogic);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
    private class computeListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            int orderId = waiterView.getOrderID();
            Order order = restaurantLogic.searchOrder(orderId);
            try {
                restaurantLogic.computeBill(order);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
    }
    private class orderViewListener implements ActionListener{


        public void actionPerformed(ActionEvent e) {

            Map<Order, List<MenuItem>> mapItems = restaurantLogic.getMapItems();
            waiterView.refresh(mapItems);
        }
    }


    private class newOrderListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {

            Map<Order, List<MenuItem>> mapItems = restaurantLogic.getMapItems();

            restaurantLogic.createNewOrder();
            waiterView.refresh(mapItems);
            final NewOrderOptionPane ord = new NewOrderOptionPane();

            ord.getAdditem().addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {

                    Map<Order, List<MenuItem>> mapItems = restaurantLogic.getMapItems();
                    int orderId = ord.getOrderID();
                    Order searchedOrder = restaurantLogic.searchOrder(orderId);
                    int menuitemId = ord.getProductID();
                    MenuItem searchedItem = restaurantLogic.searchProduct(menuitemId);

                    restaurantLogic.addItemToOrder(searchedOrder,searchedItem);
                    waiterView.refresh(mapItems);

                }
            });
            ord.getExit().addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ord.dispose();
                }
            });

        }
    }

    private class newItemListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            if (adminView.getBaseProduct().isSelected() && !adminView.getCompositeProduct().isSelected()) {
                System.out.println("BaseProd selected!");

                final BaseProductOptionPane baseprod = new BaseProductOptionPane();

                baseprod.getCreate().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Inside Listener " + baseprod.getItemName() + " " + baseprod.getPrice());
                        restaurantLogic.createBaseProduct(baseprod.getItemName(), baseprod.getPrice());
                        adminView.refresh(restaurantLogic.getMenuItems());
                        baseprod.dispose();
                    }
                });


            }
            if (!adminView.getBaseProduct().isSelected() && adminView.getCompositeProduct().isSelected()) {
                System.out.println("CompProd selected!");

                final CompositeProductOptionPane1 comp1 = new CompositeProductOptionPane1();
                final CompositeProductOptionPane2 comp2 = new CompositeProductOptionPane2();

                comp1.getCreate().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        int id = restaurantLogic.createCompositeProduct(comp1.getItemName());
                        adminView.refresh(restaurantLogic.getMenuItems());
                    }
                });

                comp2.add(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        restaurantLogic.addToComposite(comp2.getCompId(), restaurantLogic.searchProduct(comp2.getBaseId()));
                        adminView.refresh(restaurantLogic.getMenuItems());
                    }
                });

                comp2.exit(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        comp1.dispose();
                        comp2.dispose();
                    }
                });

            }
        }
    }
    private class editItemListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("EDIT");

            if (adminView.getBaseProduct().isSelected()) {
                System.out.println("BaseProd selected!");

                final EditBaseProductOptionPane baseprod = new EditBaseProductOptionPane();
                baseprod.getEdit().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {

                        restaurantLogic.editMenuItem(baseprod.getId(),baseprod.getProductName(),baseprod.getPrice(),1F,1);
                        adminView.refresh(restaurantLogic.getMenuItems());
                    }
                });
                baseprod.getExit().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        baseprod.dispose();
                    }
                });
            }
            if (adminView.getCompositeProduct().isSelected()){
                final EditCompositeProductOptionPane comp1 = new EditCompositeProductOptionPane();


                comp1.getEdit().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {

                        restaurantLogic.editMenuItem(comp1.getCompId(),comp1.getProductName(),comp1.getPrice(),1F,1);
                        adminView.refresh(restaurantLogic.getMenuItems());
                    }
                });
                comp1.getExit().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        comp1.dispose();
                    }
                });
                comp1.getRemove().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {

                        restaurantLogic.removeFromComposite(comp1.getCompId(),restaurantLogic.searchProduct(comp1.getBaseId()));
                        adminView.refresh(restaurantLogic.getMenuItems());
                    }
                });

            }
            if (adminView.getCompositeProduct().isSelected()==false && adminView.getBaseProduct().isSelected() == false){
                System.out.println("No product type selected");
            }
        }
    }
    private class deleteItemListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("DELETE");

            restaurantLogic.deleteMenuItem(adminView.getId());
            adminView.refresh(restaurantLogic.getMenuItems());
        }
    }
    private class viewItemListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            System.out.println("VIEW ITEMS");
            adminView.refresh(restaurantLogic.getMenuItems());

        }
    }

    public static void main(String[] args) {
        try {
            new RestaurantController();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
