package PresentationLayer;

import javax.swing.*;
import java.awt.*;

public class NewOrderOptionPane extends JFrame {

    JPanel panel =  new JPanel(new GridLayout(4,0));

    JLabel order = new JLabel("Order ID:");
    JTextField orderTF = new JTextField("",5);
    JLabel itemId = new JLabel("MenuItem ID:");
    JTextField itemTF = new JTextField("",5);
    JLabel tableId = new JLabel("Table No. :");
    JTextField tableTF = new JTextField("",5);

    JButton additem = new JButton("Add to table");
    JButton exit = new JButton("Exit");


    public NewOrderOptionPane(){

        panel.add(order);
        panel.add(orderTF);
        panel.add(itemId);
        panel.add(itemTF);
        panel.add(tableId);
        panel.add(tableTF);
        panel.add(additem);
        panel.add(exit);

        this.add(panel);

        Dimension baseProdDim = new Dimension(400,150);
        this.setSize(baseProdDim);
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new NewOrderOptionPane();
    }

    public JButton getAdditem() {
        return additem;
    }

    public JButton getExit() {
        return exit;
    }

    public int getOrderID() {
        return Integer.parseInt(orderTF.getText());
    }
    public int getProductID(){
        return Integer.parseInt(itemTF.getText());
    }
}
