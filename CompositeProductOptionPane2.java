package PresentationLayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CompositeProductOptionPane2 extends JFrame {

    JPanel panel = new JPanel();
    JLabel compProd = new JLabel("CompositeProduct ID:");
    JTextField idComp = new JTextField("",5);
    JLabel baseProd = new JLabel("BaseProduct ID:");
    JTextField idfield = new JTextField("",5);
    JButton add = new JButton("Add");
    JButton exit = new JButton("Exit");
    public CompositeProductOptionPane2(){

        GridLayout gridLayout = new GridLayout(3,0);

        panel.setLayout(gridLayout);
        panel.add(compProd);
        panel.add(idComp);
        panel.add(baseProd);
        panel.add(idfield);
        panel.add(add);
        panel.add(exit);
        this.add(panel);

        this.setSize(new Dimension(400,100));
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2-200,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


    }


    public void exit(ActionListener e){
        exit.addActionListener(e);
    }
    public void add(ActionListener e){
        add.addActionListener(e);
    }

    public int getCompId() {
        return Integer.parseInt(idComp.getText());
    }

    public int getBaseId() {
        return Integer.parseInt(idfield.getText());
    }

    public static void main(String[] args) {
        new CompositeProductOptionPane2();
    }
}
