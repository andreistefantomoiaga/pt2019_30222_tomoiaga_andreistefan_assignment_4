package BusinessLayer;

import java.io.Serializable;

public class MenuItem implements Serializable {

    private int id;
    private String productName;
    private float price;
    private float kcal;
    private int grammage;

    public MenuItem(){

        id= this.hashCode();
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice() {
        return price;
    }

    public void setKcal(float kcal) {
        this.kcal = kcal;
    }

    public float getKcal() {
        return kcal;
    }

    public int getGrammage() {
        return grammage;
    }

    public void setGrammage(int grammage) {
        this.grammage = grammage;
    }

    public float computePrice() {
        return price;
    }


    public boolean equalProperties(MenuItem menuItem) {

        if(this.getProductName() == menuItem.getProductName() && this.getPrice() == menuItem.getPrice() && this.getKcal() == menuItem.getKcal() && this.getGrammage() == menuItem.getGrammage()){
            return true;
        }
        return false;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }




}
