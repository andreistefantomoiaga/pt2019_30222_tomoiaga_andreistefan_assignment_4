package PresentationLayer;

import javax.swing.*;
import java.awt.*;

public class EditCompositeProductOptionPane extends JFrame {

    JPanel panel = new JPanel(new GridLayout(8,0));

    JLabel idlab = new JLabel("CompositeProduct ID:");
    JLabel idlab2 = new JLabel("BaseProduct ID:");
    JLabel name = new JLabel("ProductName: ");
    JLabel price = new JLabel("Price: ");
    JLabel kcal = new JLabel("KCAL: ");
    JLabel grammage = new JLabel("Grammage: ");

    JTextField gramTF = new JTextField("",5);
    JTextField idTF = new JTextField("",5);
    JTextField kcalTF = new JTextField("0",5);
    JTextField priceTF = new JTextField("0",5);
    JTextField nameTF = new JTextField("",5);
    JTextField id2TF = new JTextField("",5);

    JButton edit = new JButton("Edit");
    JButton exit = new JButton("Exit");
    JButton remove = new JButton("Remove from CompositeProduct");

    public EditCompositeProductOptionPane(){

        panel.add(idlab);
        panel.add(idTF);
        panel.add(name);
        panel.add(nameTF);
        panel.add(price);
        panel.add(priceTF);
        panel.add(kcal);
        panel.add(kcalTF);
        panel.add(grammage);
        panel.add(gramTF);

        panel.add(edit);
        panel.add(exit);
        panel.add(idlab2);
        panel.add(id2TF);
        panel.add(remove);




        this.add(panel);

        Dimension baseProdDim = new Dimension(500,200);
        this.setSize(baseProdDim);
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    public JButton getEdit() {
        return edit;
    }
    public JButton getExit() {
        return exit;
    }
    public JButton getRemove() {
        return remove;
    }

    public int getCompId(){
        return Integer.parseInt(idTF.getText());
    }
    public int getBaseId(){
        return Integer.parseInt(id2TF.getText());
    }
    public String getProductName(){
        return nameTF.getText();
    }
    public float getPrice(){
        return Float.parseFloat(priceTF.getText());
    }

    public static void main(String[] args) {
        new EditCompositeProductOptionPane();
    }

}
