package PresentationLayer;

import BusinessLayer.*;
import BusinessLayer.MenuItem;
import Controller.RestaurantController;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AdministratorGUI extends JFrame{

    JPanel mainPanel = new JPanel(new GridLayout(1, 2));
    JPanel pane1 = new JPanel();

    JPanel buttonPanel;
    JPanel tablePanel;

    JButton newItem;
    JButton editItem;
    JButton deleteItem;
    JButton viewMenuItems;
    JButton serialize;

    JCheckBox baseProduct;
    JCheckBox compositeProduct;

    JTextField itemName;
    JTextField itemPrice;
    JTextField itemPreparationTime;

    JLabel nameLabel = new JLabel("Item Name");
    JLabel priceLabel = new JLabel("$");
    JLabel preparationLabel = new JLabel("Prep. Time");

    JScrollPane scrollPane;
    JTable menuItemTable = new JTable();


    JLabel idLab = new JLabel("ID:");
    JTextField id = new JTextField("",3);
    BaseProductOptionPane optionPaneBaseProd;

    public AdministratorGUI(){

        this.setTitle("Administrator");

        baseProduct = new JCheckBox("BaseProduct");
        compositeProduct = new JCheckBox("CompositeProduct");

        buttonPanel = new JPanel();
        tablePanel = new JPanel();

        itemName = new JTextField();
        itemPrice = new JTextField();
        itemPreparationTime = new JTextField();

        newItem = new JButton("New Item");
        editItem = new JButton("Edit Item");
        deleteItem = new JButton("Delete Item");
        viewMenuItems = new JButton("View Menu Items");
        serialize = new JButton("Serialize");
        scrollPane = new JScrollPane();

        init();


        this.setSize(1000,500);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void refresh(List<MenuItem> all){

        System.out.println("Clicked");
        tablePanel.removeAll();

        menuItemTable  = createTable(all);
        scrollPane = new JScrollPane(menuItemTable);
        tablePanel.add(scrollPane);

        tablePanel.revalidate();
        tablePanel.repaint();

    }

    private void init(){

        others();
        mainPanel.add(buttonPanel,0,0);
        mainPanel.add(tablePanel,0,1);

        JPanel panel2 = new JPanel(new FlowLayout());
        JPanel panel3 = new JPanel(new FlowLayout());

        BoxLayout boxLayout = new BoxLayout(buttonPanel,BoxLayout.Y_AXIS);
        buttonPanel.setLayout(boxLayout);
        buttonPanel.add(Box.createVerticalGlue());
        buttonPanel.add(newItem);
        buttonPanel.add(Box.createVerticalGlue());
        buttonPanel.add(editItem);
        buttonPanel.add(Box.createVerticalGlue());
        buttonPanel.add(panel3);
        panel3.add(deleteItem);
        panel3.add(idLab);
        panel3.add(id);
        buttonPanel.add(panel2);
        panel2.add(baseProduct);
        panel2.add(compositeProduct);
        buttonPanel.add(viewMenuItems);
        buttonPanel.add(Box.createVerticalGlue());
        buttonPanel.add(serialize);
        newItem.setAlignmentX(Component.CENTER_ALIGNMENT);
        editItem.setAlignmentX(Component.CENTER_ALIGNMENT);
        deleteItem.setAlignmentX(Component.CENTER_ALIGNMENT);
        viewMenuItems.setAlignmentX(Component.CENTER_ALIGNMENT);
        baseProduct.setAlignmentX(Component.LEFT_ALIGNMENT);
        compositeProduct.setAlignmentX(Component.LEFT_ALIGNMENT);
        tablePanel.add(scrollPane);


        add(mainPanel);

    }
    private void others(){

        tablePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        buttonPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

    }

    public void newItem(ActionListener a){
        newItem.addActionListener(a);
    }

    public void selectionEvent(MouseListener e){
        System.out.println("HERE2");
        menuItemTable.addMouseListener(e);
    }
    public void editItem(ActionListener a){
        editItem.addActionListener(a);
    }
    public void deleteItem(ActionListener a){
        deleteItem.addActionListener(a);
    }
    public void viewItem(ActionListener a){
        viewMenuItems.addActionListener(a);
    }
    public void serialize(ActionListener e){serialize.addActionListener(e);}

    public JCheckBox getBaseProduct() {
        return baseProduct;
    }

    public JCheckBox getCompositeProduct() {
        return compositeProduct;
    }

    public JTable getMenuItemTable() {
        return menuItemTable;
    }

    public int getId() {
        return Integer.parseInt(id.getText());
    }

    public JTable createTable(List<MenuItem> objects){

        java.util.List<String> columnNames = new ArrayList<String>();
        List<String[]> data = new ArrayList<String[]>();

        for (Field field: MenuItem.class.getDeclaredFields()) {
            columnNames.add(field.getName());
            System.out.println(field.getName());
        }



        for (MenuItem iterator : objects) {
            data.add(new String[]{String.valueOf(iterator.getId()),iterator.getProductName(),String.valueOf(iterator.getPrice()),String.valueOf(iterator.getKcal()),String.valueOf(iterator.getGrammage())});
        }


        TableModel tableModel= new DefaultTableModel(data.toArray(new Object[][]{}),columnNames.toArray());
        JTable retunedJtable =new JTable(tableModel);

        return retunedJtable;
    }

    public static void main(String[] args) {
        AdministratorGUI agui = new AdministratorGUI();

    }

}
