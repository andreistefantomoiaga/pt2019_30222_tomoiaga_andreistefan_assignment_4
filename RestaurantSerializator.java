package DataLayer;

import BusinessLayer.Restaurant;

import java.io.*;

public class RestaurantSerializator {

    Restaurant rest;

    public  RestaurantSerializator(){
    }

    public void serializeRestaurant(Restaurant restaurant) throws IOException {

        FileOutputStream fileOut = new FileOutputStream("restaurant.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(restaurant);
        out.close();
        fileOut.close();
        System.out.printf("Serialized data is saved in restaurant.ser");
    }
    public void deserializeRestaurant() throws IOException, ClassNotFoundException {

        FileInputStream fileIn = new FileInputStream("restaurant.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        rest = (Restaurant) in.readObject();
        in.close();
        fileIn.close();
    }

    public Restaurant getRestaurant() {
        return rest;
    }

    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();
        RestaurantSerializator restaurantSerializator = new RestaurantSerializator();
        try {
            restaurantSerializator.serializeRestaurant(restaurant);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
