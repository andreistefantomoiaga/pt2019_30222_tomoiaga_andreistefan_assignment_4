package PresentationLayer;


import BusinessLayer.Observer;
import BusinessLayer.Order;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

//Observer
public class ChefGUI extends Observer implements Serializable {

    JFrame frame;
    JPanel mainPanel = new JPanel(new FlowLayout());
    JScrollPane scrollPane;
    JTextArea orderLog ;

    public ChefGUI(){
        frame = new JFrame("Chef");
        orderLog = new JTextArea("",20,30);
        scrollPane = new JScrollPane(orderLog);

        scrollPane.setSize(new Dimension(200,200));
        orderLog.setSize(new Dimension(200,200));


        mainPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        mainPanel.add(scrollPane);
        Dimension dim = new Dimension(400,400);
        mainPanel.setSize(dim);
        frame.setSize(dim);
        frame.add(mainPanel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocation(500,500);

    }


    public void update(Order order) {

        orderLog.append("Order :"+order.getOrderId()+" at "+order.getTable()+" created at " +order.getDateString());
        orderLog.append("\n");
    }

    public static void main(String[] args) {
        new ChefGUI();
    }
}
