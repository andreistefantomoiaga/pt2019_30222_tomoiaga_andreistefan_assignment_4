package BusinessLayer;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.List;

public class Restaurant implements RestaurantProcessing, Serializable {

    List<MenuItem> menuItems;
    List<Order> orderList;
    Map<Order, List<MenuItem>> mapItems;
    Observer observer;

    public Restaurant(){

        menuItems = new ArrayList<MenuItem>();
        orderList = new ArrayList<Order>();
        mapItems = new HashMap<Order, List<MenuItem>>();

    }


    public void createBaseProduct(String productName, float price) {

        if ( price < 0.0) throw new IllegalArgumentException("Price cannot be negative");

        MenuItem createdItem = new BaseProduct(productName,price,1,1);
        menuItems.add(createdItem);
    }

    public int createCompositeProduct(String productName) {
        MenuItem createdItem = new CompositeProduct(productName);
        menuItems.add(createdItem);

        assert !createdItem.equals(null);

        return createdItem.getId();
    }
    public void addToComposite(int id,MenuItem addedItem){

        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");

        CompositeProduct compositeProduct = null;

        for (MenuItem item: menuItems) {
            if(item.getId() == id){
                compositeProduct = (CompositeProduct) item;
                break;
            }
        }

        assert !compositeProduct.equals(null);
        compositeProduct.addComponent(addedItem);

    }

    public void removeFromComposite(int id,MenuItem removedItem){

        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");
        CompositeProduct compositeProduct = null;

        for (MenuItem item: menuItems) {
            if(item.getId() == id){
                compositeProduct = (CompositeProduct) item;
                break;
            }
        }
        compositeProduct.removeComponent(removedItem);

    }

    public MenuItem searchProduct(int id){

        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");
        for (MenuItem item: menuItems) {
            if(item.getId() == id){
                return item;
            }
        }

        assert 1==1:"A product should always be found!";
        return null;
    }

    public Order searchOrder(int id){

        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");
        for (Order ord: orderList) {
            if (ord.getOrderId() == id){
                return ord;
            }
        }
        assert 1==1:"An order should always be found!";
        return null;
    }

    public void deleteMenuItem(int id) {
        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");
        for (MenuItem menuItem: menuItems) {
            if(menuItem.getId() == id){
                menuItems.remove(menuItem);
                return;
            }
        }
    }

    public void editMenuItem(int id,String editProductName, float editPrice, float editKcal, int editGrammage) {

        if(id < 0) throw  new IllegalArgumentException("Id cannot be negative");
        for (MenuItem menuItem: menuItems) {
            if(menuItem.getId() == id){
                menuItem.setProductName(editProductName);
                menuItem.setPrice(editPrice);
                menuItem.setKcal(editKcal);
                menuItem.setGrammage(editGrammage);
                return;
            }
        }
        assert false: "editMenuItem didn't find the product!";
    }

    ////////////////////////WAITER////////////////////////

    public Order createNewOrder() {

        List<MenuItem> orderItems = new ArrayList<MenuItem>();

        Order newOrder = new Order();
        mapItems.put(newOrder,orderItems);
        orderList.add(newOrder);
        observer.update(newOrder);
        return newOrder;

    }
    public void addItemToOrder(Order order,MenuItem menuItem){

        if(order.equals(null) || menuItem.equals(null) )  throw  new IllegalArgumentException("Id cannot be negative");

        mapItems.get(order).add(menuItem);

    }

    public void computeBill(Order ord) throws FileNotFoundException, UnsupportedEncodingException {

        List<MenuItem> orderItems = mapItems.get(ord);

        float billValue = 0;
        PrintWriter writer = new PrintWriter("bill.txt", "UTF-8");

        for (MenuItem item: orderItems) {

            writer.println("Item name: "+item.getProductName()+" $:" + item.getPrice());
            billValue+=item.getPrice();

        }
        writer.println("Total: "+ billValue+ "$");
        writer.close();
    }

    public int computePrice(Order order) {

        int computedPrice=0;
       List<MenuItem> fetchedItems = mapItems.get(order);

        for (MenuItem item: fetchedItems) {
            computedPrice+=item.computePrice();
        }
        return computedPrice;
    }

    public void generateBill(Order order) {

    }

    public Map<Order, List<MenuItem>> getMapItems() {
        return mapItems;
    }

    public void setObserver(Observer observer) {
        this.observer = observer;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }


}
