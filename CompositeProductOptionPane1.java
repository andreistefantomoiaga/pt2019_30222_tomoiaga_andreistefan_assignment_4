package PresentationLayer;


import sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl;

import javax.swing.*;
import java.awt.*;

public class CompositeProductOptionPane1 extends JFrame {

    JPanel panel = new JPanel();
    JButton create = new JButton("Create");
    JTextField name = new JTextField("",10);
    JLabel label = new JLabel("Composite item name:");

    public CompositeProductOptionPane1(){

        FlowLayout layout = new FlowLayout();

        panel.setLayout(layout);
        panel.add(label);
        panel.add(name);
        panel.add(create);
        this.add(panel);

        this.setSize(new Dimension(400,100));
        Dimension center = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(center.width/2 - this.getWidth()/2 +200,center.height/2 - this.getHeight()/2);

        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public JButton getCreate() {
        return create;
    }


    public String getItemName() {
        return name.getText();
    }

    public static void main(String[] args) {
        new CompositeProductOptionPane1();
    }
}
